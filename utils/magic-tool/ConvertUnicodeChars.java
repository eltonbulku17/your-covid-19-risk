import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.System.exit;

/**
 * Convert a file in UTF-8 with unicode chars in <U+code> format to a file with the actual char in a new UTF-16 file.
 */
public class ConvertUnicodeChars {

  private static final Charset UTF_8 = Charset.forName( "UTF-8" );
  private static final Charset UTF_16 = Charset.forName( "UTF-16" );

  public static void main( String[] args ) {
    System.out.println( args[ 0 ] );
    if ( args.length != 1 && args.length != 2 ) {
      System.err.println( "Usage \"java -cp . ConvertUnicodeChars <input_file> [true|false]\"." );
      System.err.println( "The second parameter is optional, and if it is true, the first ; is replace with =" );
      exit( 0 );
    }

    String regexp = "<U\\+([0-9a-fA-F]+)>";

    Pattern pattern = Pattern.compile( regexp );

    File fileInput = new File( args[ 0 ] );
    if ( !fileInput.exists() ) {
      System.err.println( "Input file " + args[ 1 ] + " was not found" );
    }
    try ( FileInputStream fis = new FileInputStream( fileInput ) ) {
      try ( InputStreamReader isr = new InputStreamReader( fis, StandardCharsets.UTF_8 ) ) {
        try ( BufferedReader br = new BufferedReader( isr ) ) {
          try ( FileOutputStream fos = new FileOutputStream( args[ 0 ] + ".output" ) ) {
            try ( OutputStreamWriter stw = new OutputStreamWriter( fos, StandardCharsets.UTF_16 ) ) {
              try ( BufferedWriter bw = new BufferedWriter( stw ) ) {
                String line = null;
                while ( ( line = br.readLine() ) != null ) {
                  Matcher match = null;
                  do {
                    match = pattern.matcher( line );
                    while ( match.find() ) {
                      int unicode = Integer.parseInt( match.group( 1 ), 16 );
                      String replacement = Character.toString( (char) unicode );
                      line = line.replaceFirst( regexp, replacement );
                    }
                  } while( match != null && match.matches() );

                  // convert ; to = if requested
                  if ( args.length > 1 && args[ 1 ].equalsIgnoreCase( Boolean.toString( true ) ) ) {
                    line = line.replaceFirst( ";", "=" );
                  }

                  // write line
                  bw.write( line + "\n" );
                }
              }
            }
          }
        }
      }
    } catch ( Exception e ) {
      e.printStackTrace();
    }
  }
}