rootPath <- here::here("v1");
dataPath <- file.path(rootPath, "data");
scriptPath <- file.path(rootPath, "scripts");
dctPath <- file.path(rootPath, "operationalizations", "dct-files");

limesurveySheetsPath <- file.path(rootPath, "operationalizations", "sheets");
limesurveyPath <- file.path(rootPath, "operationalizations", "limesurvey");

abcdsPath <- file.path(rootPath, "abcds");

workingPath <- file.path(rootPath, "intermediate-results");

translationOutputPath <- file.path(rootPath, "translations");

websiteSheetsPath <- file.path(translationOutputPath, "sheets");
websitePath <- file.path(rootPath, "website");

i18nOutputPath_test <- file.path(websitePath, "i18n_test");
i18nOutputPath_stge <- file.path(websitePath, "i18n_stge");
i18nOutputPath_prod <- file.path(websitePath, "i18n");
