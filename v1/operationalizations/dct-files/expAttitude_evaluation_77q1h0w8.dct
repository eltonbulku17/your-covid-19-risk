---
dct:
  version: 0.1.0
  id: expAttitude_evaluation_77q1h0w8
  label: "Experiential attitude belief evaluation"
  date: "2020-04-24"
  ancestry: "expAttitude_evaluation_73dnt5z2"       ### The DCT(s) this DCT is based on
  retires:         ### Any DCT(s) that are made obsolete by this one
  source:
    id: "raa_book"

################################################################################

  definition:
    definition: "An experiential attitude belief evaluation is the evaluation as positive or negative (i.e. desirable or undesirable) of one specific experiential potential consequence of a behavior. The experiential nature of this evaluation means that this concerns mostly expected experiences and sensations, such as pleasure or pain. Consequences that render the target behavior less or more instrumental given more long-term goals are captured in instrumental attitude and the underlying beliefs (see dct:expAttitude_expectation_73dnt5z1 and dct:instrAttitude_expectation_73dnt5z6 for more details about this distinction).

According to the Reasoned Action Approach, experiential attitude belief evaluations combine multiplicatively with experiential attitude belief expectations (see dct:expAttitude_expectation_73dnt5z1) into experiential attitude beliefs (see dct:expAttitude_belief_73dnt5z3).

Examples of experiential attitude belief evaluations are (note that the examples provided for the experiential attitude belief expectations should probably be read first, see dct:expAttitude_expectation_73dnt5z1): the degree to which one finds it pleasant to sit down during a bus journey to the city centre; the evaluation of watching the scenery as desirable; one's evaluation of a pizza's taste as nice; one appraising a relaxed feeling as pleasant; one enjoying the sensation of sex without a condom."

    source:
      spec: ""

################################################################################

  measure_dev:
    instruction: "Experiential attitude belief evaluations are measured using questions in a questionnaire that are referred to as 'items'. Experiential attitude belief evaluation always relate to specific experiential attitude belief expectations, so the first step is to identify the expectation for which the evaluation should be measured (e.g. 'Drinking alcohol makes me feel ... [much more relaxed|much more excited]'). Evaluations of expectations are always measured on a bidimensional scale.
	
As item stem, use 'I prefer ...', and as anchors, include the scale extremes used when measuring the expectation. For example, an item could be 'I prefer ... [being much more relaxed|being much more excited]'. Make sure to formulate the item stem such that it makes clear that you are asking people about *their* evaluation (not what they think holds more generally). Use a seven-point response scale and try to always be consistent in the scale valence; in languages that are read from left to right, always place the most passive/low/less/weak/unlikely scale extreme (anchor) on the left, and the most active/high/more/strong/likely scale extreme (anchor) on the right. Do not reverse this order for one or more items.

The item or set of items should be accompanied by an instruction that makes clear that you are asking people about *their* evaluation (not what they think holds more generally).

For attribute continuums where one extreme end carries a societally normative load, for example relating to sacrosanct topics or taboos (e.g. death, sex, childhood, injury, psychopathology, or violations of personal integrity), it can be necessary to avoid items that allow registering responses indicating desirability of those attributes that a society deems deviant. Such cases may necessitate deviating from full alignment with the expectation measure, instead embedding one of the extremes in the item stem.

In such cases, if there is a fear of negative responses to one half of the bidimensional scale that spans the full attribute continuum, this can often be addressed by embedding the extreme that is most consistent with societal norms in the item stem. For example, when measuring distance keeping in the context of a pandemic, people can expect that doing so can make them feel either selfish (because one does this to keep oneself safe) or altruistic (because one does this to keep others safe). The associated evaluation maps this expectation onto a valence from extremely negative to extremely positive. Both evaluations can feasibly be held. People may prefer feeling selfish over feeling altruistic (because one holds certain libertarian or individualistic ideals), or people may prefer feeling altruistic over feeling selfish (because one holds certain humanistic or communitarianistic ideals). However, nonetheless, concerns about a target population's responses to the possibility that people may want to feel selfish can be a reason to avoid measuring that dimension, and in that case, the societally less deviant extreme can be embedded in the item stem. In this example, that would mean embedding 'feeling altruistic' in the item stem.

In these two cases, the item stem should contain the relevant scale extreme, as well as signal that it is about one's own evaluation. The typical item to use in English would be 'For me, [SCALE EXTREME] is… [ very undesirable | very desirable ]', where sometimes, the scale extreme is negated by using a term similar to avoiding: 'For me, avoiding [SCALE EXTREME] is… [ very undesirable | very desirable ]'

To apply this to the example scenario, the item would become for example 'For me, feeling altruistic is… [ very undesirable | very desirable ]'.

However, in some cases, it is the deviant extreme where the most important variation is expected, for example when addressing feelings of guilt or shame. To stick to the same example of keeping one's distance, people may expect that this makes them feel very lonely (the spatial distance can easily be associated to social or personal distance), or people may expect that this makes them feel very sociable (because shared remarkable experiences often prompt social interaction, and keeping distance can feasibly be seen as an example of this). In terms of evaluation, people may evaluate either end of the spectrum as desirable or undesirable, but in many societies, loneliness carries negative connotations, while at the same time people's fear of loneliness may be where the important variation is expected. Here, embedding the societally accepted continuum extreme in the item stem (the degree to which people like feeling sociable) would prohibit direct labeling of the part of the attribute continuum where the important variation is expected (the degree to which people dislike feeling lonely). Fortunately, such scenarios can be resolved with a similar approach as used in the first case: by including the societally deviant continuum extreme in the item stem, but also including the desire to avoid that.

This yields items such as 'For me, avoiding feeling lonely is… [ very undesirable | very desirable ]'

Often, embedding one scale extreme in the item stem will resolve concerns relating to societal norms. However, in some cases, that may not be enough, and a further concession is necessary: sometimes, using bidimensional scales may not be possible at all. For example, in this specific example, it may be felt that the possibility of wanting to feel lonely is still too salient in the item. In such cases, the societally deviant half of the attribute continuum can be forgone by collapsing that half into the lowest answer option. In that case, try to find a lower anchor label that clearly expresses neutrality, to avoid differential interpretation of that anchor (with some people interpreting it as the opposite of the high anchor, and some people interpreting it as neutral). For example, in English, the anchors ''not particularly desirable' and 'extremely desirable' can be used. The makes clear that 'not particularly desirable', and not the scale midpoint, expresses absence of a strong evaluation in the positive direction (or, ideally, any positive valence to one's evaluation).

This would change the item from the example above to 'For me, avoiding feeling lonely is… [ not particularly desirable | extremely desirable ]'."

    source:
      spec: ""

################################################################################

  measure_code:
    instruction: "Experiential attitude belief evaluations are measured using questions in a questionnaire (these are referred to as 'items'). Items can be coded as measuring an experiential attitude belief evaluation if they measure participants' evaluation of one specific experiential potential consequence of a behavior as positive or negative (i.e. desirable or undesirable).

The experiential nature of this evaluation means that this concerns mostly expected experiences and sensations, such as pleasure or pain.

The question should include, either in its stem or in its anchors, the specific dimension of which the evaluation is being measured (e.g. whether people prefer feeling relaxed or feeling excited, or whether people evaluate being excited as positive or negative).

Note that evaluations of consequences that render the target behavior less or more instrumental given more long-term goals are captured in instrumental attitude and the underlying beliefs. (see dct:expAttitude_expectation_73dnt5z1 and dct:instrAttitude_expectation_73dnt5z6 for more details about this distinction, and see dct:instrAttitude_evaluation_73dnt5z7 for the coding instruction for instrumental attitude belief evaluations)."

    source:
      spec: ""

################################################################################

  manipulate_dev:
    instruction: ""
    source:
      spec: ""

################################################################################

  manipulate_code:
    instruction: ""
    source:
      spec: ""

################################################################################

  aspect_dev:
    instruction: "Conduct a qualitative study where participants are interviewed, and the interviews are either recorded and transcribed, or notes are kept. These sources (i.e. transcripts or notes) are then coded using the instruction for aspect coding. In this qualitative study, use these questions: 'What do you see as the advantages of you engaging in [target behavior]?', 'What do you see as the disadvantages of you engaging in [target behavior]?', and 'What else comes to mind when you think about [target behavior]?'."

    source:
      spec: ""

################################################################################

  aspect_code:
    instruction: "Expressions of one's evaluations as positive or negative (i.e. desirable or undesirable) of experiential potential consequences of a behavior. The experiential nature of this evaluation means that this these evaluations concern mostly expected experiences and sensations, such as pleasure or pain.

Note that expressions of evaluations of consequences that render the target behavior less or more instrumental given more long-term goals are captured in instrumental attitude and the underlying beliefs (see dct:instrAttitude_evaluation_73dnt5z7).

One's actual expectation (i.e. whether a consequence is likely or unlikely) should be coded as dct:expAttitude_expectation_73dnt5z1."

    source:
      spec: ""

################################################################################

  rel:
    id: "expAttitude_belief_73dnt5z3"
    type: "causal_influences_product"

################################################################################
---

